﻿using LAB04.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAB04
{
    public partial class Form1 : Form
    {
        StudentContexDB context;
        public Form1()
        {
           
            InitializeComponent();
            context = new StudentContexDB();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
        private void BindGrid(List<Student> listStudent)
        {
            dgvStudent.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dgvStudent.Rows.Add();
                dgvStudent.Rows[index].Cells[0].Value = item.StudentID;
                dgvStudent.Rows[index].Cells[1].Value = item.FullName;
                if (item.Faculty != null)
                {
                    dgvStudent.Rows[index].Cells[2].Value = item.Faculty.FacultyName;

                }
                dgvStudent.Rows[index].Cells[3].Value = item.AverageScore;
            }
        }

        private void FillFalcultyCombobox(List<Faculty> listFalcultys)
        {
            this.cmbKhoa.DataSource = listFalcultys;
            this.cmbKhoa.DisplayMember = "FacultyName";
            this.cmbKhoa.ValueMember = "FacultyID";
            //this.cmbKhoa.SelectedIndex = 0;

        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
           
            try
            {
                StudentContexDB context = new  StudentContexDB();
                List<Faculty> listFacultys = context.Faculties.ToList();
                List<Student> listStudents = context.Students.ToList();
                FillFalcultyCombobox(listFacultys);
                BindGrid(listStudents);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private int GetSelecctedRow(string studentID)
        {
            for (int i = 0; i < dgvStudent.Rows.Count; i++)
            {
                if (dgvStudent.Rows[i].Cells[0].Value.ToString() == studentID)
                {
                    return i;
                }
            }
            return -1;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {

       
            try
            {
               

                if (txtMSSV.Text == "" || txtHoTen.Text == "" || txtDTB.Text == "")
                {
                    if (txtMSSV.Text == "")
                    {

                        MessageBox.Show("Loix");
                    }
                    if (txtHoTen.Text == "")
                    {
                        MessageBox.Show("Loix");
                    }
                    if (txtDTB.Text == "")
                    {
                        MessageBox.Show("Loix");
                    }
                    throw new Exception("Vui lòng nhập đầy đủ thông tin sinh viên!");
                }
                else if (IsStudentID(txtMSSV.Text) == false || txtHoTen.Text.Length < 3 || txtHoTen.Text.Length > 100 || float.Parse(txtDTB.Text) > 10)
                {
                    if (IsStudentID(txtMSSV.Text) == false)
                    {
                        MessageBox.Show("Mã sinh viên gồm 10 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    }
                    if (txtHoTen.Text.Length < 3 || txtHoTen.Text.Length > 100)
                    {
                        MessageBox.Show("Tên sinh viên gồm 3 - 100 ký tự", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                      
                    }
                    if (float.Parse(txtDTB.Text) > 10)
                    {
                        MessageBox.Show("Điểm trung bình từ 0 - 10", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                       
                    }
                }

                else
                {
                    try
                    {
                        StudentContexDB context = new StudentContexDB();
                        Student s = new Student()
                        {
                            StudentID = txtMSSV.Text,
                            FullName = txtHoTen.Text,
                            AverageScore = double.Parse(txtDTB.Text),
                            FacultyID = (cmbKhoa.SelectedItem as Faculty).FacultyID
                        };
                        context.Students.Add(s);

                        context.SaveChanges();

                        BindGrid(context.Students.ToList());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
    

        private void btnSua_Click(object sender, EventArgs e)
        {
            StudentContexDB context = new StudentContexDB();
            Student dbUpdate = context.Students.FirstOrDefault(p => p.StudentID == txtMSSV.Text);
            if (dbUpdate != null)
            {
                dbUpdate.FullName = txtHoTen.Text;
                dbUpdate.AverageScore = double.Parse(txtDTB.Text);
                dbUpdate.FacultyID = (cmbKhoa.SelectedItem as Faculty).FacultyID;

                context.SaveChanges(); 

                BindGrid(context.Students.ToList());
            }
            else
            {
                MessageBox.Show("không tìm thấy sinh viên");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            StudentContexDB context = new StudentContexDB();
            Student dbDelete = context.Students.FirstOrDefault(p => p.StudentID == txtMSSV.Text);
            if (dbDelete != null)
            {
                context.Students.Remove(dbDelete);
                context.SaveChanges();
                BindGrid(context.Students.ToList());
                MessageBox.Show("Xoá sinh viên thành công");

            }
            else
            {
                MessageBox.Show("không tìm thấy sinh viên");
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {

            DialogResult rs = MessageBox.Show("Thoát chương trình", "Thông báo", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                Application.Exit();

            }
        }

        private void dgvStudent_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvStudent.Rows[e.RowIndex];
                txtMSSV.Text = row.Cells[0].Value.ToString();
                txtHoTen.Text = row.Cells[1].Value.ToString();
                cmbKhoa.Text = row.Cells[2].Value.ToString();
                txtDTB.Text = row.Cells[3].Value.ToString();
            }
        }


        private void thoatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        private void txtMSSV_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private bool IsStudentID(string studentID)
        {
            return studentID.Length == 10;
        }
       
    }
}
